import {solveWithJavaScript, solveWithWebAssembly} from "./solver";

const SIZE = 9;

export class Sudoku {
    constructor(twoDimensionalArray) {
        if (twoDimensionalArray) {
            this.array = twoDimensionalArray;
        } else {
            this.array = [];
            for (let i = 0; i < SIZE; i++) {
                this.array.push(new Array(SIZE).fill(0));
            }
        }
    }

    solve(useJavascriptAlgorithm = false) {
        return new Promise((resolve, reject) => {
            let solve = useJavascriptAlgorithm ? solveWithJavaScript : solveWithWebAssembly;
            const start = performance.now();

            solve(this)
                .then(solution => resolve({
                    sudoku: new Sudoku(solution),
                    passedSeconds: (performance.now() - start) / 1000
                })).catch(e => reject(e));
        });
    }

    clear() {
        for (let y = 0; y < SIZE; y++) {
            for (let x = 0; x < SIZE; x++) {
                this.array[y][x] = 0;
            }
        }
    }

    clone() {
        const clone = [];
        for (let y = 0; y < SIZE; y++) {
            for (let x = 0; x < SIZE; x++) {
                if (x === 0) clone.push([]);
                clone[y][x] = this.array[y][x];
            }
        }

        return new Sudoku(clone);
    }
}
