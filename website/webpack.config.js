const path = require('path');

module.exports = {
  experiments: {
    asyncWebAssembly: true,
  },
  entry: "./index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.js",
  },
  mode: "development"
};

